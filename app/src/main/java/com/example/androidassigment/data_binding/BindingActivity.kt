package com.example.androidassigment.data_binding

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassigment.Model
import com.example.androidassigment.R
import com.example.androidassigment.generic_adapter.MainAdapter

class BindingActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: GenericAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_binding)

        viewAdapter = GenericAdapter(getDummyList())
        viewManager = LinearLayoutManager(this)

        // Creates a vertical Layout Manager
        recyclerView = findViewById<RecyclerView>(R.id.recyclerview).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }


    fun getDummyList(): ArrayList<Model> {
        var list = ArrayList<Model>()
        var random = 0;
        for (x in 0 until 50) {
            random = (0..1).random()
            if (random == 0) {
                list.add(
                    Model(
                        R.drawable.cartoon,
                        "test $x",
                        R.layout.b_item1
                    )
                )
            } else {
                list.add(
                    Model(
                        R.drawable.cartoon,
                        "test $x",
                        R.layout.b_item2
                    )
                )
            }
        }

        return list
    }

}