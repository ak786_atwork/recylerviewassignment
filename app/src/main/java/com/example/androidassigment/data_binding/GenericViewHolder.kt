package com.example.androidassigment.data_binding

import android.graphics.drawable.Drawable
import android.media.Image
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassigment.Model

open class GenericViewHolder : RecyclerView.ViewHolder {
    lateinit var data: Model
    lateinit var itemViewBinding: ViewDataBinding
    constructor(itemViewBinding: ViewDataBinding): super(itemViewBinding.root) {
        this.itemViewBinding = itemViewBinding
    }

    public fun bind(data: Model) {
        this.data = data
        itemViewBinding.setVariable(BR.model, data)
        itemViewBinding.executePendingBindings()
    }



}
