package com.example.androidassigment

import android.widget.ImageView
import androidx.databinding.BindingAdapter


@BindingAdapter("image")
fun setImage(imageView : ImageView, drawableId: Int) {
    imageView.setImageResource(drawableId)
}

public data class Model(val resourceId: Int, val name : String, val viewType: Int) {

}