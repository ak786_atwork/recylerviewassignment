package com.example.androidassigment.generic_adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassigment.Model
import java.util.ArrayList

class MainAdapter(private val list: ArrayList<Model>) : RecyclerView.Adapter<MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return ViewHolderFactory.getViewHolderByViewType(viewType,parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bindView(list[position])
    }

    override fun getItemViewType(position: Int): Int {
        return list[position].viewType
    }
}




