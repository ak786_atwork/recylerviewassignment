package com.example.androidassigment.generic_adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewParent
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassigment.R
import java.lang.IllegalArgumentException

class ViewHolderFactory {
    companion object {
        public fun getViewHolderByViewType(viewType : Int, parent: ViewGroup) : MainViewHolder {
            return when(viewType) {
                R.layout.item1 -> ViewHolder1(LayoutInflater.from(parent.context).inflate(viewType,parent,false))
                R.layout.item2 -> ViewHolder2(LayoutInflater.from(parent.context).inflate(viewType,parent,false))
                else -> throw IllegalArgumentException("ViewHolder doesn't exists")
            }
        }
    }
}