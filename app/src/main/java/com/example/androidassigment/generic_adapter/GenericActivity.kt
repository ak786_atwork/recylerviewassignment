package com.example.androidassigment.generic_adapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassigment.Model
import com.example.androidassigment.R

class GenericActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MainAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generic_acitivity)

        viewAdapter = MainAdapter(getDummyList())
        viewManager = LinearLayoutManager(this)

        // Creates a vertical Layout Manager
        recyclerView = findViewById<RecyclerView>(R.id.recyclerview).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }


    fun getDummyList(): ArrayList<Model> {
        var list = ArrayList<Model>()
        var random = 0;
        for (x in 0 until 50) {
            random = (0..1).random()
            if (random == 0) {
                list.add(
                    Model(
                        0,
                        "test $x",
                        R.layout.item1
                    )
                )
            } else {
                list.add(
                    Model(
                        R.drawable.cartoon,
                        "test $x",
                        R.layout.item2
                    )
                )
            }
        }

        return list
    }

}