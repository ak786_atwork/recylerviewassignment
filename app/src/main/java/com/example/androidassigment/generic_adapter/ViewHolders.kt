package com.example.androidassigment.generic_adapter

import android.view.View
import com.example.androidassigment.Model
import kotlinx.android.synthetic.main.item1.view.*
import kotlinx.android.synthetic.main.item2.view.*

class ViewHolder1(itemView: View) : MainViewHolder(itemView) {
    val nameTextView = itemView.nameTextView

    override fun bindView(data: Model) {
        //bind data to item1 layout
        nameTextView.text = data.name
    }
}

class ViewHolder2(itemView: View) : MainViewHolder(itemView) {
    val imageView = itemView.imageView

    override fun bindView(data: Model) {
        //bind data to item2 layout
        imageView.setImageResource(data.resourceId)
    }
}