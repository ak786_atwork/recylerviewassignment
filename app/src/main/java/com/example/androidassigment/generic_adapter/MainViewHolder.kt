package com.example.androidassigment.generic_adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassigment.Model

abstract class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bindView(data : Model)
}