package com.example.androidassigment.general

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassigment.R
import kotlinx.android.synthetic.main.item1.view.*
import java.util.ArrayList

class TestAdapter(private val animals: ArrayList<String>) : RecyclerView.Adapter<TestAdapter.TestViewHolder>() {


//    constructor(animals: ArrayList<String>) {
//        this.animals = animals
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(viewType, parent, false)

        return TestViewHolder(view)
    }

    override fun getItemCount(): Int {
        return animals.size
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: TestViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.nameTextView.text = animals[position]
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item1
    }


    class TestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Holds the TextView
        val nameTextView = itemView.nameTextView
    }
}




