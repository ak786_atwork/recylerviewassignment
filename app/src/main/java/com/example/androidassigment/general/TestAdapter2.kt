package com.example.androidassigment.general

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassigment.R
import kotlinx.android.synthetic.main.item1.view.*
import kotlinx.android.synthetic.main.item2.view.*
import java.util.ArrayList

class TestAdapter2(private val animals: ArrayList<String>, private val context : Context) : RecyclerView.Adapter<TestAdapter2.TestViewHolder2>() {


//    constructor(animals: ArrayList<String>) {
//        this.animals = animals
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestViewHolder2 {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item2, parent, false)

        return TestViewHolder2(view)
    }

    override fun getItemCount(): Int {
        return animals.size
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: TestViewHolder2, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.imageView.setImageResource(R.drawable.cartoon)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item2
    }


    class TestViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Holds the TextView
        val imageView = itemView.imageView
    }
}




