package com.example.androidassigment.general

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class MyMergeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder> {

    lateinit var adapterList : ArrayList<RecyclerView.Adapter<RecyclerView.ViewHolder>>
    private var size = 0
    private var count = 0

    constructor(vararg adapters: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
        for (adapter in adapters) {
            adapterList.add(adapter)
            size += adapter.itemCount
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return adapterList[count % adapterList.size].onCreateViewHolder(parent,viewType);
    }

    override fun getItemCount(): Int {
        return size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return adapterList[count++ % adapterList.size].onBindViewHolder(holder,position)
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

}


